# API Gateway

Primary goals:
- It provides a central point where all calls to services are received and redirected to the correct service provider.

Implementing a new service route.

1 - First of all we have to have the latest version of the project
2 - Edit 'nginx.conf'. (For this manual we are going to use 'backend-moment-service' as example')
We make a copy from another service, (Ex. subscriptions), and change the parameters with those of the new service.
In our case we change the url for '/api/v1/moments' and the name of service for 'http://moment-service'.
```
    location /api/v1/moments {
        proxy_set_header Host $host;
        proxy_redirect off;
        proxy_pass http://moment-service;
        }
```
3 - Create a new tag, ask to which version we can use, in our case we created "v1.2.0".
4 - Push changes to the repository.

### Deploy Release

1 - Goto Jenkins -> Backing -> Api Gateway -> release
- Press Build with Parameters and introduce the tag, in our case "v1.2.0"
- Press Build.

### Do Deploy

1 - Jenkins -> Devops -> Docker Deploy Kubernetes
- SERVICE: nginx
- IMAGE_VERSION: 1.2.0
- ENVIRONMENT: dev
